/*
 * garagedooropener.h
 *
 *  Created on: 03.11.2015
 *      Author: chris
 */

#ifndef SRC_APP_FSM_INCLUDE_GARAGEDOOROPENER_H_
#define SRC_APP_FSM_INCLUDE_GARAGEDOOROPENER_H_


typedef enum { 	STATE_UP = 0,
				STATE_DOWN,
				STATE_MOVE_UP,
				STATE_MOVE_DOWN,
				STATE_STOP_FROM_UP,
				STATE_STOP_FROM_DOWN,
				STATE_ERROR,
				STATE_MAX
} State_t;

typedef enum {
	EV_BUTTON_PRESS = 0,
	EV_LIMIT_SWITCH_TOP,
	EV_LIMIT_SWITCH_DOWN,
	EV_MAX
} Event_t;

typedef void (*stateHandler_t)(void);

typedef struct {
	State_t nextState;
	stateHandler_t hdl;
}StateTuple_t;

State_t handleEvent(State_t currentState,Event_t ev);

#endif /* SRC_APP_FSM_INCLUDE_GARAGEDOOROPENER_H_ */
