/*
 * garagedooropener.c
 *
 *  Created on: 03.11.2015
 *      Author: chris
 */


#include "garagedooropener.h"
#include <stdio.h>

void stopMotor()
{
	printf("-Stopping motor \n");
}

void driveUP()
{
	printf("/\\ moving up \n");
}
void driveDown()
{
	printf("\\/ moving down \n");
}
void errorMode()
{
	printf("X Stopping motor. > Error detected <  \n");
}

static StateTuple_t TransitionTable[EV_MAX][STATE_MAX] = {
		                  /*  STATE_UP,                    STATE_DOWN               STATE_MOVE_UP                   STATE_MOVE_DOWN                   STATE_STOP_FROM_UP           STATE_STOP_FROM_DOWN     STATE_ERROR              */
/* EV_BUTTON_PRESS      */	{ {STATE_MOVE_DOWN,driveDown}, {STATE_MOVE_UP,driveUP}, {STATE_STOP_FROM_UP,stopMotor}, {STATE_STOP_FROM_DOWN,stopMotor}, {STATE_MOVE_DOWN,driveDown}, {STATE_MOVE_UP,driveUP}, {STATE_MOVE_UP,driveUP}}, /* EV_BUTTON_PRESS      */
/* EV_LIMIT_SWITCH_TOP  */  { {STATE_UP,stopMotor},        {STATE_ERROR,errorMode}, {STATE_UP,stopMotor},           {STATE_ERROR,errorMode},          {STATE_ERROR,errorMode},     {STATE_ERROR,errorMode}, {STATE_ERROR,errorMode}}, /* EV_LIMIT_SWITCH_TOP  */
/* EV_LIMIT_SWITCH_DOWN */  { {STATE_ERROR,errorMode},     {STATE_DOWN,stopMotor},  {STATE_ERROR,errorMode}, 		{STATE_DOWN,stopMotor},           {STATE_ERROR,errorMode},     {STATE_ERROR,errorMode}, {STATE_ERROR,errorMode}}, /* EV_LIMIT_SWITCH_DOWN */
};



State_t handleEvent(State_t currentState,Event_t ev)
{
	State_t ret = STATE_ERROR;
	if(   ((STATE_UP <= currentState) && (STATE_MAX > currentState))
		&& ((EV_BUTTON_PRESS) <= ev && ( EV_MAX > ev)) )
	{
		StateTuple_t state = TransitionTable[ev][currentState];
		state.hdl();
		ret =  state.nextState;
	}
	else
	{
		errorMode();
	}
	return ret;
}
