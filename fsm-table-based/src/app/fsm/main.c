
#include <stdio.h>
#include "garagedooropener.h"

static Event_t events[] = {
		EV_LIMIT_SWITCH_TOP,
		EV_BUTTON_PRESS,
		EV_BUTTON_PRESS,
		EV_BUTTON_PRESS,
		EV_LIMIT_SWITCH_TOP,
		EV_BUTTON_PRESS,
		EV_LIMIT_SWITCH_DOWN
};

char* eventNames [EV_MAX] = {
		"o button pressed",
		"` upper limit switch toggled",
		", lower limit switch toggled"
};

int main(void)
{
	State_t state = STATE_MOVE_UP;

	for(unsigned int i = 0; i < sizeof(events)/sizeof(Event_t);i++ )
	{
		printf("%s\n",eventNames[events[i]]);
		state = handleEvent(state,events[i]);
	}

	return 0;
}
