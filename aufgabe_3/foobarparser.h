/**
 * @file fooparser.h
 * This program have been written as a solution to the first programming exercise
 * Copyright (C) 2015  Christian Ege
 */

/*
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */


#ifndef foobarparser_h
#define foobarparser_h

#include <stdio.h>
#include <stdlib.h>

/** @brief State definitions */
typedef enum {
    STATE_START = 0,
    STATE_CHAR_A,
    STATE_COLON_A,
    STATE_A,
    STATE_CLOSING_ANGLE_BRACKET_A,
    STATE_CHAR_SPACE,
    STATE_CHAR_B,
    STATE_COLON_B,
    STATE_B,
    STATE_CLOSING_ANGLE_BRACKET_B,
    STATE_END_OF_LINE,
    STATE_END_OF_FILE,
    STATE_ERROR,
    STATE_MAX
} State_t;

/** @brief Event definitions */
typedef enum {
    EV_CHAR_A = 0,
    EV_CHAR_B,
    EV_CHAR_COLON,
    EV_CHAR_OPENEN_ANGLE_BRACKET,
    EV_CHAR_CLOSING_ANGLE_BRACKET,
    EV_NUMBER,
    EV_SPACE,
    EV_END_OF_LINE,
    EV_END_OF_FILE,
    EV_ERROR,
    EV_MAX
} Event_t;

typedef enum {
    STATUS_SUCCESS = 0,
    STATUS_FAILURE,
    STATUS_EOF
} Result_t;

/**
 * @brief Holds the meta state of the statemachine
 */
typedef struct {
    State_t state; /*< The state   */
    int valueA;    /*< Value for A */
    int valueB;    /*< Value for B */
} MyState_t;

/** 
 * @brief Create a foobar parser statemachine instance
 *
 * @return The statemachine
 */
MyState_t* foobarparser_create(void);

/** 
 * @brief Execute the statemachine
 * 
 * @param statemachine The instance of the statemachine
 * @param event The current event
 * @param c The current character
 *
 * @return Failure or success
 */
Result_t foobarparser_handle(MyState_t* statemachine, Event_t event, char c);

/**
 * @brief Frees a foobar parser statemachine instance
 */
void foobarparser_free(MyState_t* statemachine);

/**
 * @brief Classify the input character
 *
 * @return The Event
 */
Event_t foobarparser_classify(char c);


#endif /* foobarparser_h */
