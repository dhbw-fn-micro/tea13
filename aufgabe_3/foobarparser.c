/**
 * @file fooparser.c
 * This program have been written as a solution to the first programming exercise
 * Copyright (C) 2015  Christian Ege
 */

/*
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */


#include "foobarparser.h"
#include <string.h>

int handleA(MyState_t* state,char c)
{
    int ret = EXIT_FAILURE;
    if(NULL != state)
    {
        state->valueA *= 10;
        state->valueA += c - '0';
        ret = EXIT_SUCCESS;
    }
    return ret;      
}

int handleB(MyState_t* state,char c)
{
    int ret = EXIT_FAILURE;
    if(NULL != state)
    {
        state->valueB *= 10;
        state->valueB += c - '0';
        ret = EXIT_SUCCESS;
    }
    return ret;
}

int saveA(MyState_t* state,char c)
{
    return EXIT_SUCCESS;
}

int saveB(MyState_t* state,char c)
{
    return EXIT_SUCCESS;
}

int none(MyState_t* state,char c)
{
    return EXIT_SUCCESS;
}

int println(MyState_t* state,char c)
{
    printf("A:[%d] B:[%d] \n",state->valueA,state->valueB);
    state->valueA = 0;
    state->valueB = 0;
    return EXIT_SUCCESS;
}

int error(MyState_t* state,char c)
{
    printf("Error detected! \n");
    return EXIT_FAILURE;
}

typedef int (*stateHandler_t)(MyState_t*,char);

typedef struct {
    State_t nextState;
    stateHandler_t hdl;
}StateTuple_t;

static StateTuple_t TransitionTable[EV_MAX][STATE_MAX] = {
    /*                                    STATE_START           STATE_CHAR_A          STATE_COLON_A        STATE_A                                STATE_CLOSING_ANGLE_BRACKET_A  STATE_CHAR_SPACE     STATE_CHAR_B           STATE_COLON_B  STATE_B  STATE_CLOSING_ANGLE_BRACKET_B          STATE_END_OF_LINE              STATE_END_OF_FILE          STATE_ERROR  */
    /* EV_CHAR_A                     */ { {STATE_CHAR_A,none},  {STATE_ERROR,error},  {STATE_ERROR,error}, {STATE_ERROR,error},                   {STATE_ERROR,error},           {STATE_ERROR,error}, {STATE_ERROR,error},   {STATE_ERROR,error},    {STATE_ERROR,error},                   {STATE_ERROR,error},           {STATE_CHAR_A,none},       {STATE_ERROR,error}, {STATE_ERROR,error} },
    /* EV_CHAR_B                     */ { {STATE_ERROR,error},  {STATE_ERROR,error},  {STATE_ERROR,error}, {STATE_ERROR,error},                   {STATE_ERROR,error},           {STATE_CHAR_B,none}, {STATE_ERROR,error},   {STATE_ERROR,error},    {STATE_ERROR,error},                   {STATE_ERROR,error},           {STATE_ERROR,error},       {STATE_CHAR_A,none}, {STATE_ERROR,error} },
    /* EV_CHAR_COLON                 */ { {STATE_ERROR,error},  {STATE_COLON_A,none}, {STATE_ERROR,error}, {STATE_ERROR,error},                   {STATE_ERROR,error},           {STATE_ERROR,error}, {STATE_COLON_B,none},  {STATE_ERROR,error},    {STATE_ERROR,error},                   {STATE_ERROR,error},           {STATE_ERROR,error},       {STATE_ERROR,error}, {STATE_ERROR,error} },
    /* EV_CHAR_OPENEN_ANGLE_BRACKET  */ { {STATE_ERROR,error},  {STATE_ERROR,error},  {STATE_A,none},      {STATE_ERROR,error},                   {STATE_ERROR,error},           {STATE_ERROR,error}, {STATE_ERROR,error},   {STATE_B,none},         {STATE_ERROR,error},                   {STATE_ERROR,error},           {STATE_ERROR,error},       {STATE_ERROR,error}, {STATE_ERROR,error} },
    /* EV_CHAR_CLOSING_ANGLE_BRACKET */ { {STATE_ERROR,error},  {STATE_ERROR,error},  {STATE_ERROR,error}, {STATE_CLOSING_ANGLE_BRACKET_A,saveA}, {STATE_ERROR,error},           {STATE_ERROR,error}, {STATE_ERROR,error},   {STATE_ERROR,error},    {STATE_CLOSING_ANGLE_BRACKET_B,saveB}, {STATE_ERROR,error},           {STATE_ERROR,error},       {STATE_ERROR,error}, {STATE_ERROR,error} },
    /* EV_NUMBER                     */ { {STATE_ERROR,error},  {STATE_ERROR,error},  {STATE_ERROR,error}, {STATE_A,handleA},                     {STATE_ERROR,error},           {STATE_ERROR,error}, {STATE_ERROR,error},   {STATE_ERROR,error},    {STATE_B,handleB},                     {STATE_ERROR,error},           {STATE_ERROR,error},       {STATE_ERROR,error}, {STATE_ERROR,error} },
    /* EV_SPACE                      */ { {STATE_ERROR,error},  {STATE_ERROR,error},  {STATE_ERROR,error}, {STATE_ERROR,error},                   {STATE_CHAR_SPACE,none},       {STATE_ERROR,error}, {STATE_ERROR,error},   {STATE_ERROR,error},    {STATE_ERROR,error},                   {STATE_ERROR,error},           {STATE_ERROR,error},       {STATE_ERROR,error}, {STATE_ERROR,error} },
    /* EV_END_OF_LINE                */ { {STATE_ERROR,error},  {STATE_ERROR,error},  {STATE_ERROR,error}, {STATE_ERROR,error},                   {STATE_ERROR,error},           {STATE_ERROR,error}, {STATE_ERROR,error},   {STATE_ERROR,error},    {STATE_ERROR,error},                   {STATE_END_OF_LINE,println},   {STATE_ERROR,error},       {STATE_ERROR,error}, {STATE_ERROR,error} },
    /* EV_END_OF_FILE                */ { {STATE_ERROR,error},  {STATE_ERROR,error},  {STATE_ERROR,error}, {STATE_ERROR,error},                   {STATE_ERROR,error},           {STATE_ERROR,error}, {STATE_ERROR,error},   {STATE_ERROR,error},    {STATE_ERROR,error},                   {STATE_ERROR,error},           {STATE_END_OF_FILE,none},  {STATE_ERROR,error}, {STATE_ERROR,error} },
    /* EV_ERROR                      */ { {STATE_ERROR,error},  {STATE_ERROR,error},  {STATE_ERROR,error}, {STATE_ERROR,error},                   {STATE_ERROR,error},           {STATE_ERROR,error}, {STATE_ERROR,error},   {STATE_ERROR,error},    {STATE_ERROR,error},                   {STATE_ERROR,error},           {STATE_ERROR,error},       {STATE_ERROR,error}, {STATE_ERROR,error} } 
};


MyState_t* foobarparser_create(void)
{
    MyState_t* ret = (MyState_t*)malloc(sizeof(MyState_t));
    if(NULL != ret)
    {
        memset(ret,0,sizeof(MyState_t));
        ret->state = STATE_START;
    }
    return ret;
}

void foobarparser_free(MyState_t* statemachine)
{
    if(NULL != statemachine)
    {
        free(statemachine);
    }
}

Event_t foobarparser_classify(char c)
{
    Event_t ret = EV_ERROR;
    if('0' <= c && '9' >= c)
    {
        ret = EV_NUMBER;
    } 
    else if(' ' == c)
    {
        ret = EV_SPACE;
    }
    else if('A' == c)
    {
        ret = EV_CHAR_A;
    }
    else if('B' == c)
    {
        ret = EV_CHAR_B;
    }
    else if(':' == c)
    {
        ret = EV_CHAR_COLON;
    }
    else if('<' == c)
    {
        ret = EV_CHAR_OPENEN_ANGLE_BRACKET;
    }
    else if('>' == c)
    {
        ret = EV_CHAR_CLOSING_ANGLE_BRACKET;
    }
    else if('\n' == c)
    {
        ret = EV_END_OF_LINE;
    }
    else if(EOF == c)
    {
        ret = EV_END_OF_FILE;
    }
    return ret;
}

Result_t foobarparser_handle(MyState_t* statemachine, Event_t event, char c)
{
    int ret = STATUS_FAILURE;
    if(   ((STATE_START <= statemachine->state) && (STATE_MAX > statemachine->state))
        && ((EV_CHAR_A) <= event && ( EV_MAX > event)) )
    {
        StateTuple_t state = TransitionTable[event][statemachine->state];
        ret = state.hdl(statemachine,c);
        statemachine->state = state.nextState;
    }
    return ret;
}