/**
 * @file main.c
 * This program have been written as a solution to the first programming exercise
 * Copyright (C) 2015  Christian Ege
 */

/*
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */


#include <stdio.h>
#include "foobarparser.h"

static const char* in_filename = "/tmp/foobar.txt"; /**< input file change for M$ based OS */

int main(int argc, const char * argv[]) {

    int ret = EXIT_SUCCESS;;
    Result_t res = STATUS_FAILURE;
    FILE * in_file_hdl = NULL;
    
    /* create a statemachine handle */
    MyState_t* state = foobarparser_create();
    
    in_file_hdl = fopen (in_filename,"r");
    
    if ((NULL == in_file_hdl))
    {
        printf("Error while opening file: %s \n",in_filename);
        ret = EXIT_FAILURE;
    }
    else
    {
        while((0 == feof(in_file_hdl)))
        {
            char c = fgetc(in_file_hdl);
            if(STATUS_SUCCESS != (res = foobarparser_handle(state, foobarparser_classify(c),c)))
            {
                /* end of file reached or error */
                if(res != STATUS_EOF)
                {
                    ret = EXIT_FAILURE;
                }
                break;
            }
        }
    }
    
    if(NULL != in_file_hdl)
    {
        fclose(in_file_hdl);
    }
    foobarparser_free(state);
    return ret;
}
