/**
 * @file main.c
 * This program have been written as a solution to the first programming exercise
 * Copyright (C) 2015  Christian Ege
 */

/*
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef myLinkedList_h
#define myLinkedList_h

#include <stdint.h>
/**
 * @struct listNode
 * @brief This struct defines a list element or
 *        somethimes called a list node.
 */
typedef struct listNode
{
    /** @brief Pointer to previous Element */
    struct listNode *pPrev;
    /** @brief Pointer to next Element */
    struct listNode *pNext;
    /** @brief Operant A for the MAC Operation */
    uint32_t operand_a;
    /** @brief Operant B for the MAC Operation */
    uint32_t operand_b;
    /** @brief Result the MAC Operation */
    uint64_t result;
} listNode_t ;

typedef struct listNodeMem
{
    listNode_t node;
    uint32_t inUse;
} listNodeMem_t;


/**
 * @struct doubleLinkedList
 * @brief Helper struct to hold the head and Tail of the list
 */
typedef struct doubleLinkedList
{
    /** @brief Pointer to the head of the list. NULL if list is empty. */
    listNode_t *headOfList;
    /** @brief Pointer to the tail of the list. NULL if list is empty. */
    listNode_t *tailOfList;
}doubleLinkedList_t;

/**
 * @brief create a new list Element and setup the initial values
 *
 * @param [in] operand_a The operand a for the MAC operation
 * @param [in] operand_b The operand b for the MAC operation
 * @param [in] result The result of the MAC operation
 * @result he new element or NULL in case of error
 */
listNode_t* list_get_new_element(uint32_t operand_a,uint32_t operand_b,uint64_t result);

/**
 * @brief free a list Node this returns the memory to the os
 *
 * @param elem [in] Pointer to a list node
 */
void list_free_element(const listNode_t* elem);

/**
 * @brief insert element before another one
 *
 * @param list [in] The list where the element should be linked in
 * @param old [in] The element where to insert in front of.
 * @param elem [in] The element to be added
 * @return EXIT_SUCCESS in case of success otherwise EXIT_FAILURE
 */
int list_insert_before(doubleLinkedList_t *list , listNode_t* old ,listNode_t* elem);

/**
 * @brief insert element after another one
 *
 * @param list [in] The list where the element should be linked in
 * @param old [in] The element where to insert after.
 * @param elem [in] The element to be added
 * @return EXIT_SUCCESS in case of success otherwise EXIT_FAILURE
 */
int list_insert_after(doubleLinkedList_t *list , listNode_t* old ,listNode_t* elem);

/**
 * @brief Add element to the beginning of the list.
 *
 * @param list [inout] list where element should be appended to.
 * @param elem [in] element to be appended to the list
 * @return EXIT_SUCCESS in case of success
 */
int list_push_front(doubleLinkedList_t *list , listNode_t* elem);

/**
 * @brief Add element to the end of the list.
 *
 * @param list [inout] list where element should be appended to.
 * @param elem [in] element to be appended to the list
 * @return EXIT_SUCCESS in case of success
 */
int list_push_back(doubleLinkedList_t *list, listNode_t* elem);


/**
 * @brief Calculates S_n = S_n-1 + A * B
 *
 * @param operand_a [in] Operand A
 * @param operand_b [in] Operand B
 * @param pre_result [in] Previous result (S_n-1)
 *
 * @return result of the operation.
 */
uint64_t do_mac_operation(int32_t operand_a,uint32_t operand_b,uint64_t prev_result);


#endif /* myLinkedList_h */
