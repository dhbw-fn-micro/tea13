/**
 * @file main.c
 * This program have been written as a solution to the first programming exercise
 * Copyright (C) 2015  Christian Ege
 */

/*
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "myLinkedList.h"
#include "string.h"
#include <stdlib.h>

listNode_t* list_get_new_element(uint32_t operand_a,uint32_t operand_b,uint64_t result)
{
    listNode_t* ret = NULL;
    ret = (listNode_t*)malloc(sizeof(listNode_t));
    if(NULL != ret )
    {
        ret->operand_a = operand_a;
        ret->operand_b = operand_b;
        ret->result = result;
        ret->pPrev = NULL;
        ret->pNext = NULL;
    }
    return ret;
}

int list_push_back(doubleLinkedList_t *list, listNode_t* elem)
{
    if ( NULL == elem )
    {
        return EXIT_FAILURE;
    }
    if(NULL == list->tailOfList)
    {
        return list_push_front(list,elem);
    }
    else
    {
        return list_insert_after(list, list->tailOfList, elem);
    }
}

int list_push_front(doubleLinkedList_t *list , listNode_t* elem)
{
    if(NULL == elem )
    {
        return EXIT_FAILURE;
    }
    if ( NULL == list->headOfList )
    {
        list->headOfList = elem;
        list->tailOfList  = elem;
        elem->pPrev = NULL;
        elem->pNext = NULL;
        return EXIT_SUCCESS;
    }
    else
    {
        return  list_insert_before(list, list->headOfList, elem);
    }
}

int list_insert_after(doubleLinkedList_t *list , listNode_t* old ,listNode_t* elem)
{
    elem->pPrev = old;
    elem->pNext = old->pNext;
    
    if(( NULL == old ) ||  (NULL == elem ) )
    {
        return EXIT_FAILURE;
    }
    if( NULL ==  old->pNext )
    {
        list->tailOfList = elem;
    }
    else
    {
        old->pNext->pPrev = elem; /* already checked for NULL !*/
    }
    old->pNext = elem;
    
    return EXIT_SUCCESS;
}

int list_insert_before(doubleLinkedList_t *list , listNode_t* old ,listNode_t* elem)
{
    int ret = EXIT_FAILURE;
    
    elem->pPrev = old->pPrev;
    elem->pNext = old;
    
    if(( NULL == old ) ||  (NULL == elem ) )
    {
        return EXIT_FAILURE;
    }
    if( NULL == old->pPrev )
    {
        list->headOfList = elem;
    }
    else
    {
        old->pPrev->pNext = elem; /* already checked for NULL */
    }
    old->pPrev  = elem;
    
    return ret;
}

void list_free_element(const listNode_t* elem)
{
    if( NULL != elem )
        free((void*)elem);
}

