/**
 * @file
 * This program have been written as a solution to the first programming exercise
 * Copyright (C) 2015  Christian Ege
 */

/*
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "myStatemachine.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

static const char* in_filename = "/tmp/foobar.txt"; /**< input file change for M$ based OS */

int main(int argc, const char * argv[]) {

    int ret = EXIT_FAILURE;
    FILE * in_file_hdl = NULL;
    
    /* create a statemachine handle */
    MyState_t* state = myStatemachine_create();
    
    in_file_hdl = fopen (in_filename,"r");
    
    if ((NULL == in_file_hdl))
    {
        printf("Error while opening files: %s \n",in_filename);
        ret = EXIT_FAILURE;
    }
    else
    {
        while((0 == feof(in_file_hdl)))
        {
            if(0 != myStatemachine_handle(state, fgetc(in_file_hdl)))
            {
                /* end of file reached */
                break;
            }
        }
    }
    
    if(NULL != in_file_hdl)
    {
        fclose(in_file_hdl);
    }
    myStatemachine_free(state);
    return ret;
}
