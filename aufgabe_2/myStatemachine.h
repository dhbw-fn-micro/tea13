/**
 * @file
 * This program have been written as a solution to the first programming exercise
 * Copyright (C) 2015  Christian Ege
 */

/*
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */


#ifndef myStatemachine_h
#define myStatemachine_h

typedef struct MyState MyState_t; /* forward declaration of internal state */

/**
 * @brief Create a statemachine
 * 
 * @return the statemachine
 */
MyState_t* myStatemachine_create(void);

/**
 * Frees a statemachine
 *
 * @param statemachine the statemachine to free
 */
void myStatemachine_free(MyState_t* statemachine);

/**
 * @brief handle the states
 *
 * @param statemachine the statemachine to use
 * @param event the event
 * @return 0 on success
 */
int myStatemachine_handle(MyState_t* statemachine, char event);

#endif /* myStatemachine_h */
