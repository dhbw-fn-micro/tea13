/**
 * @file
 * This program have been written as a solution to the first programming exercise
 * Copyright (C) 2015  Christian Ege
 */

/*
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "myStatemachine.h"


/**
 * The inernal states
 */
typedef enum {
    STATE_START = 0,
    STATE_CHAR_A,
    STATE_COLON_A,
    STATE_A,
    STATE_CLOSING_ANGLE_BRACKET_A,
    STATE_CHAR_SPACE,
    STATE_CHAR_B,
    STATE_COLON_B,
    STATE_B,
    STATE_CLOSING_ANGLE_BRACKET_B,
    STATE_END_OF_LINE,
    STATE_END_OF_FILE,
    STATE_ERROR,
    STATE_MAX
} StatemachineState_t;

/**
 * @brief Holds the state of the statemachine
 */
struct MyState{
    StatemachineState_t state; /*< The state   */
    int valueA;                /*< Value for A */
    int valueB;                /*< Value for B */
};

MyState_t* myStatemachine_create(void)
{
    MyState_t* ret = (MyState_t*)malloc(sizeof(MyState_t));
    if(NULL != ret)
    {
        memset(ret,0,sizeof(MyState_t));
        ret->state = STATE_START;
    }
    return ret;
}

void myStatemachine_free(MyState_t* statemachine)
{
    if(NULL != statemachine)
    {
        free(statemachine);
    }
}

int myStatemachine_handle(MyState_t* statemachine, char event)
{
    int ret = 0;
    switch (statemachine->state)
    {
        case STATE_START:
            if ('A' == event)
            {
                statemachine->state = STATE_CHAR_A;
            }
            else
            {
                statemachine->state = STATE_ERROR;
                ret = 1;
            }
            break;
        case STATE_CHAR_A:
            if(':' == event)
            {
                statemachine->state = STATE_COLON_A;
            }
            else
            {
                statemachine->state = STATE_ERROR;
                ret = 1;
            }
            break;
        case STATE_COLON_A:
            if('<' == event)
            {
                statemachine->state = STATE_A;
            }
            break;
        case STATE_A:
            if(('0' <= event) && ('9' >= event) )
            {
                statemachine->state = STATE_A;
                statemachine->valueA *= 10;
                statemachine->valueA += event - '0';
            }
            else if('>' == event)
            {
                statemachine->state = STATE_CLOSING_ANGLE_BRACKET_A;
            }
            else
            {
                statemachine->state = STATE_ERROR;
                ret = 1;
            }
            break;
        case STATE_CLOSING_ANGLE_BRACKET_A:
            if(' ' == event)
            {
                statemachine->state = STATE_CHAR_SPACE;
            }
            else
            {
                statemachine->state = STATE_ERROR;
                ret = 1;
            }
            break;
        case STATE_CHAR_SPACE:
            if('B' == event)
            {
                statemachine->state = STATE_CHAR_B;
            }
            else
            {
                statemachine->state = STATE_ERROR;
                ret = 1;
            }
            break;
        case STATE_CHAR_B:
            if(':' == event)
            {
                statemachine->state = STATE_COLON_B;
            }
            else
            {
                statemachine->state = STATE_ERROR;
                ret = 1;
            }
            break;
        case STATE_COLON_B:
            if('<' == event)
            {
                statemachine->state = STATE_B;
            }
            else
            {
                statemachine->state = STATE_ERROR;
                ret = 1;
            }
            break;
        case STATE_B:
            if(('0' <= event) && ('9' >= event))
            {
                statemachine->state = STATE_B;
                statemachine->valueB *= 10;
                statemachine->valueB += event - (int)'0';
            }
            else if ('>' == event)
            {
                statemachine->state = STATE_CLOSING_ANGLE_BRACKET_B;
            }
            else
            {
                statemachine->state = STATE_ERROR;
                ret = 1;
            }
            break;
        case STATE_CLOSING_ANGLE_BRACKET_B:
            if('\n' == event)
            {
                printf("A[%d] B[%d]\n",statemachine->valueA,statemachine->valueB);
                statemachine->state = STATE_END_OF_LINE;
            }
            else if(EOF == event)
            {
                statemachine->state = STATE_END_OF_FILE;
                ret = 1;
            }
            else
            {
                statemachine->state = STATE_ERROR;
                ret = 1;
            }
            break;
        case STATE_END_OF_LINE:
            if('A' == event)
            {
                statemachine->valueA = 0;
                statemachine->valueB = 0;
                statemachine->state = STATE_CHAR_A;
            }
            else if(EOF == event)
            {
                statemachine->state = STATE_END_OF_FILE;
                ret = 1;
            }
            else
            {
                statemachine->state = STATE_ERROR;
                ret = 1;
            }
            break;
        default:
            ret = 1;
            break;
    }
    return ret;
}